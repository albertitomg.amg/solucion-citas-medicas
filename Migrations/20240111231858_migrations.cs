﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace citas.Migrations
{
    /// <inheritdoc />
    public partial class migrations : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "USUARIOS",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NOMBRE = table.Column<string>(type: "varchar(255)", unicode: false, maxLength: 255, nullable: false),
                    APELLIDOS = table.Column<string>(type: "varchar(255)", unicode: false, maxLength: 255, nullable: false),
                    USUARIO = table.Column<string>(type: "varchar(255)", unicode: false, maxLength: 255, nullable: false),
                    CLAVE = table.Column<string>(type: "varchar(255)", unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_USUARIOS", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "MEDICOS",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false),
                    NUM_COLEGIADO = table.Column<string>(type: "varchar(255)", unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MEDICOS", x => x.ID);
                    table.ForeignKey(
                        name: "FK_MEDICOS_USUARIOS_ID",
                        column: x => x.ID,
                        principalTable: "USUARIOS",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PACIENTES",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false),
                    NSS = table.Column<string>(type: "varchar(255)", unicode: false, maxLength: 255, nullable: false),
                    NUM_TARJETA = table.Column<string>(type: "varchar(255)", unicode: false, maxLength: 255, nullable: false),
                    TELEFONO = table.Column<string>(type: "varchar(255)", unicode: false, maxLength: 255, nullable: false),
                    DIRECCION = table.Column<string>(type: "varchar(255)", unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PACIENTES", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PACIENTES_USUARIOS_ID",
                        column: x => x.ID,
                        principalTable: "USUARIOS",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CITAS",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FECHA_HORA = table.Column<DateTime>(type: "datetime", nullable: true),
                    MOTIVO_CITA = table.Column<string>(type: "varchar(255)", unicode: false, maxLength: 255, nullable: true),
                    Attrib11 = table.Column<int>(type: "int", unicode: false, maxLength: 255, nullable: false),
                    MEDICO_ID = table.Column<int>(type: "int", nullable: false),
                    PACIENTE_ID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__CITAS__3214EC27F6B3691D", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CITAS_MEDICO_ID",
                        column: x => x.MEDICO_ID,
                        principalTable: "MEDICOS",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CITAS_PACIENTE_ID",
                        column: x => x.PACIENTE_ID,
                        principalTable: "PACIENTES",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "MEDICOS_PACIENTES",
                columns: table => new
                {
                    MEDICO_ID = table.Column<int>(type: "int", nullable: false),
                    PACIENTE_ID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("MEDICOS_PACIENTES_PK", x => new { x.MEDICO_ID, x.PACIENTE_ID });
                    table.ForeignKey(
                        name: "FK_MEDICOS_PACIENTES_MEDICO_ID",
                        column: x => x.MEDICO_ID,
                        principalTable: "MEDICOS",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_PACIENTES_MEDICOS_PACIENTE_ID",
                        column: x => x.PACIENTE_ID,
                        principalTable: "PACIENTES",
                        principalColumn: "ID");
                });

            migrationBuilder.CreateTable(
                name: "DIAGNOSTICOS",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    VALORACION_ESPECIALISTA = table.Column<string>(type: "varchar(255)", unicode: false, maxLength: 255, nullable: false),
                    ENFERMEDAD = table.Column<string>(type: "varchar(255)", unicode: false, maxLength: 255, nullable: false),
                    CITA_ID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DIAGNOSTICOS", x => x.ID);
                    table.ForeignKey(
                        name: "FK_DIAGNOSTICOS_CITA_ID",
                        column: x => x.CITA_ID,
                        principalTable: "CITAS",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CITAS_MEDICO_ID",
                table: "CITAS",
                column: "MEDICO_ID");

            migrationBuilder.CreateIndex(
                name: "IX_CITAS_PACIENTE_ID",
                table: "CITAS",
                column: "PACIENTE_ID");

            migrationBuilder.CreateIndex(
                name: "IX_DIAGNOSTICOS_CITA_ID",
                table: "DIAGNOSTICOS",
                column: "CITA_ID");

            migrationBuilder.CreateIndex(
                name: "IX_MEDICOS_PACIENTES_PACIENTE_ID",
                table: "MEDICOS_PACIENTES",
                column: "PACIENTE_ID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DIAGNOSTICOS");

            migrationBuilder.DropTable(
                name: "MEDICOS_PACIENTES");

            migrationBuilder.DropTable(
                name: "CITAS");

            migrationBuilder.DropTable(
                name: "MEDICOS");

            migrationBuilder.DropTable(
                name: "PACIENTES");

            migrationBuilder.DropTable(
                name: "USUARIOS");
        }
    }
}
