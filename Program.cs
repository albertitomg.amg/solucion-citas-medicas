using AutoMapper;
using citas.Mapper;
using citas.Models;
using citas.Repository;
//using citas.Security;
using citas.Service;
using Microsoft.AspNetCore.Authentication;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

IConfiguration configuration = builder.Configuration;

builder.Services.AddDbContext<CitasContext>(options =>
        options.UseSqlServer(configuration.GetConnectionString("Citas")));

// Add services to the container.
builder.Services.AddRazorPages();

var mapperConfig = new MapperConfiguration(mc =>
{
    mc.AddProfile(new MappingProfile());
});

IMapper mapper = mapperConfig.CreateMapper();

builder.Services.AddSingleton(mapper);

// Add Repository, Service and other classes
//Repository
builder.Services.AddScoped<CitaRepository>();
builder.Services.AddScoped<DiagnosticoRepository>();
builder.Services.AddScoped<MedicoRepository>();
builder.Services.AddScoped<PacienteRepository>();
builder.Services.AddScoped<UsuarioRepository>();
//Service
builder.Services.AddScoped<CitaService>();
builder.Services.AddScoped<DiagnosticoService>();
builder.Services.AddScoped<MedicoService>();
builder.Services.AddScoped<PacienteService>();
builder.Services.AddScoped<UsuarioService>();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapRazorPages();

app.Run();
