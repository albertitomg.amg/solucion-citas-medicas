﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace citas.Models;

public partial class Cita
{
    public int Id { get; set; }

    public DateTime? FechaHora { get; set; }

    public string? MotivoCita { get; set; }

    public int Attrib11 { get; set; }

    public virtual ICollection<Diagnostico> Diagnosticos { get; } = new List<Diagnostico>();

    public int MedicoId { get; set; }

    public virtual Medico Medico { get; set; }

    public int PacienteId { get; set; }

    public virtual Paciente Paciente { get; set; }
}
