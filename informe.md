# Informe
.Net 7.0
Microsoft Visual Studio 2022

# Errores de compilaci�n

using citas.Security en UsuarioController.cs sin existir.
Clase JwtUtils tampoco existe por lo que no se puede hacer login

Llama a la clase CitaService como citaservice en Citas.Controller.cs

using autoMapper en vez de AutoMapper

No es necesario indicar .JwtBearer en using Microsoft.AspNetCore.Authentication;

builder.Services.AddAutoMapper no existe. Se debe a�adir mapper como Singelton

Paciente.citas se llama a veces como paciente.Citas. Se cambia el atributo de paciente a Citas.

# Errores al compilar

No se especificaba bien la injecci�n de IMapper


# Mejoras en el dise�o

A�adir Swagger para poder probar con facilidad lows endpoints

Implementar Services y Repositories como Interfaces

