﻿using citas.DTO;
using citas.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace citas.Controllers
{
    [ApiController]
    [Authorize(Roles = "Medico,Paciente")]
    [Route("[controller]")]
    public class citasController : ControllerBase
    {

        private readonly CitaService _citaservice;

        public citasController(CitaService CitaService)
        {
            _citaservice = CitaService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CitaDTO>> Getcitas()
        {
            return Ok(_citaservice.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<CitaDTO> GetCita(int id)
        {
            var cita = _citaservice.GetById(id);

            if (cita == null)
            {
                return NotFound();
            }

            return Ok(cita);
        }

        [HttpPost]
        public ActionResult<CitaDTO> CreateCita(CitaDTO citaDTO)
        {
            try
            {
                var createdCita = _citaservice.Add(citaDTO);
                return CreatedAtAction(nameof(GetCita), new { id = createdCita.Id }, createdCita);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPut("{id}")]
        public IActionResult UpdateCita(CitaDTO citaDTO)
        {
            try
            {

                _citaservice.Update(citaDTO);
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteCita(int id)
        {
            try
            {
                _citaservice.Delete(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

    }
}
