﻿using citas.Models;
//using citas.Security;
using citas.Service;
using Microsoft.AspNetCore.Mvc;

namespace citas.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsuarioController : ControllerBase
    {

        private readonly UsuarioService _usuarioService;
        //private readonly JwtUtils _jwtUtils;

        //public UsuarioController(UsuarioService usuarioService, JwtUtils jwtUtils)
        public UsuarioController(UsuarioService usuarioService)
        {
            _usuarioService = usuarioService;
            //_jwtUtils = jwtUtils;
        }


        /*
        [HttpPost("login")]
        public IActionResult Login(string username, string password)
        {
            IActionResult response = Unauthorized();

            var user = _usuarioService.GetUserByUsername(username, password);

            if (user != null)
            {
                var tokenString = _jwtUtils.GenerateJwtToken(user);
                response = Ok(new { token = tokenString });
            }

            return response;
        }
         */

    }
}
