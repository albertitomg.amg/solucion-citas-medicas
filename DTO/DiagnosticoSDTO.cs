﻿using citas.Models;

namespace citas.DTO
{
    public class DiagnosticoSDTO
    {

        public int Id { get; set; }
        public string ValoracionEspecialista { get; set; }
        public string Enfermedad { get; set; }

        public DiagnosticoSDTO() { }

        public DiagnosticoSDTO(Diagnostico diagnostico)
        {
            Id = diagnostico.Id;
            ValoracionEspecialista = diagnostico.ValoracionEspecialista;
            Enfermedad = diagnostico.Enfermedad;
        }

    }
}
