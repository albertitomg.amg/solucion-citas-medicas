﻿using citas.Models;

namespace citas.DTO
{
    public class MedicoSDTO : UsuarioDTO
    {
        public string NumColegiado { get; set; } = null!;


        public MedicoSDTO() { }

        public MedicoSDTO(Medico medico) : base(medico)
        {
            NumColegiado = medico.NumColegiado;

        }
    }
}
